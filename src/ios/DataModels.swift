/**
* DataModels.swift
* Copyright © 2021 Thales DIS. All rights reserved.
*/

import Foundation

struct PProfileCapture: Encodable {
    var name: String?
    var isSkipAllowed: Bool
    var response: [String]?
    var steps: [[String:String]]?
    
    enum CodingKeys: String, CodingKey {
       case name, isSkipAllowed, response, steps
    }
}

struct PField: Encodable {
    var format: String?
    var isDisplayed: Bool?
    var editable: [String]?
    
    enum CodingKeys: String, CodingKey {
        case format, isDisplayed, editable
    }
}

struct PScenarios: Encodable {
    var idx: String?
    var name: String?
    var captures: [PProfileCapture]?
    var responses: [String:[String:PField]]?
    var infoData: [String:String]?
    
    enum CodingKeys: String, CodingKey {
        case idx, name, captures, responses, infoData
    }
}

struct PProfileCaptor: Encodable {
    var pages: [String]?
    var metadata: [String:Any]?
    var isUploadAllowed: Bool?
    var category: String?
    
    enum CodingKeys: String, CodingKey {
        case pages, metadata, isUploadAllowed, category
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(pages, forKey: .pages)
        try container.encode(isUploadAllowed, forKey: .isUploadAllowed)
        try container.encode(category, forKey: .category)
        
        var nestedContainer = container.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: .metadata)
        if let metadata = metadata {
            try nestedContainer.encode(metadata)
        }
    }
}

struct PProfileConfigs: Encodable {
    var k1m: String?
    var k1e: String?
    var httpTimeoutSecond: String?
    var httpMaxRetryNum: String?
    var httpRetryDelaySecond: String?
    var pushNotifTimeoutSecond: String?
    var pullMaxRetryNum: String?
    var lang: String?
    var sizeLimit: String?
    
    enum CodingKeys: String, CodingKey {
        case k1m, k1e, httpTimeoutSecond, httpMaxRetryNum, httpRetryDelaySecond, pushNotifTimeoutSecond, pullMaxRetryNum, lang, sizeLimit
    }
}

struct PTdiProfileInfo: Encodable {
    var version: String?
    var scenarios: [PScenarios]?
    var captors: [String:PProfileCaptor]?
    var configs: PProfileConfigs?
    var theme: [String:Any]?
    var localization: [String:Any]?

    enum CodingKeys: String, CodingKey {
        case version, scenarios, captors, configs, theme, localization
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(version, forKey: .version)
        try container.encode(scenarios, forKey: .scenarios)
        try container.encode(captors, forKey: .captors)
        try container.encode(configs, forKey: .configs)
        
        var nestedContainer1 = container.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: .theme)
        if let theme = theme {
            try nestedContainer1.encode(theme)
        }
        
        var nestedContainer2 = container.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: .localization)
        if let localization = localization {
            try nestedContainer2.encode(localization)
        }
    }
}
