/**
 * CustomCaptor.swift
 * Custom captor using IDV SDK.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

import Foundation
import IDV_DOC
import main

enum CAPTURETYPE : Int {
    case PASSPORT
    case IDDOCUMENT
    case ICAO
    case CUSTOM
}

let DOCUMENT_TD1 = IDVDOCDocument.init(width: 85.6, andHeight: 53.98)
let DOCUMENT_TD2 = IDVDOCDocument.init(width: 105.00, andHeight: 74.00)
let DOCUMENT_TD3 = IDVDOCDocument.init(width: 125.00, andHeight: 88.00)

let DOCUMENT_MODE_IDDOCUMENT : [IDVDOCDocument?] = [DOCUMENT_TD1, DOCUMENT_TD2]
let DOCUMENT_MODE_PASSPORT : [IDVDOCDocument?] = [DOCUMENT_TD3]
let DOCUMENT_MODE_ICAO : [IDVDOCDocument?] = [DOCUMENT_TD1, DOCUMENT_TD2, DOCUMENT_TD3]

class CustomCaptor : UIViewController, IDVDOCStartCallback  {
    @IBOutlet weak var camera : UIView!
    @IBOutlet weak var captureImageView : UIImageView!
    @IBOutlet weak var stackView : UIStackView!
    @IBOutlet weak var confirmBox : UIView!
    @IBOutlet var captureView : UIView!
    @IBOutlet weak var drawOverlay : DrawOverlayView!
    @IBOutlet var lblFlashMessage : UILabel!
        
    private var captureType : CAPTURETYPE!
    private var totalSide = 1
    private var imageArray = [Data?]()
    private var currentSide = 1
    private let sdk = IDVDOCCaptureSDK.init()
    private var isMachineLearning : Bool = false
    private let config = IDVDOCConfiguration.init()
    private var configuration = [String: Any]()
    private var license : String = ""
    private var isOverLay : Bool = true
    private var jpegQuality : Float = 0.8 // 80 Percent
    
    private let EXTRA_CAPTURE_MODE = "EXTRA_CAPTURE_MODE"
    private let EXTRA_CAPTURE_NB_SIDES = "EXTRA_CAPTURE_NB_SIDES"
    private let EXTRA_CAPTURE_OVERLAY = "EXTRA_CAPTURE_OVERLAY"
    private let EXTRA_IDV_LICENSE = "EXTRA_IDV_LICENSE"
    private let EXTRA_CAPTURE_DETECTION_MODE = "EXTRA_CAPTURE_DETECTION_MODE"
    private let EXTRA_CAPTURE_QUALITY_CHECK_BLUR = "EXTRA_CAPTURE_QUALITY_CHECK_BLUR"
    private let EXTRA_CAPTURE_QUALITY_CHECK_GLARE = "EXTRA_CAPTURE_QUALITY_CHECK_GLARE"
    private let EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS = "EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS"
    private let EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY = "EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY"
    private let EXTRA_DOC_SIZE = "EXTRA_DOC_SIZE"
    private let EXTRA_CAPTURE_JPEG_COMPRESSION = "EXTRA_CAPTURE_JPEG_COMPRESSION"
    
    @objc var m_callbackId: String?
    @objc var m_commandDelegate: CDVCommandDelegate!
    private var m_input : InputImpl?
    private var m_pageDict = [AnyHashable:Any]()
    // input = InputImpl.init
    
    override func viewDidLoad() {
        PLog(self, "captureType= \(String(describing: captureType))")
        PLog(self, "pages= \(String(describing: configuration[EXTRA_CAPTURE_NB_SIDES]))")
        PLog(self, "license1= \(String(describing: configuration[EXTRA_IDV_LICENSE]))")
        
        captureImageView.isHidden = true
        confirmBox.isHidden = true //stackView.isHidden = true
        lblFlashMessage.layer.borderWidth = 1.0
        lblFlashMessage.layer.borderColor = UIColor.white.cgColor
        setupConfirmBox()
        configDocument()
        configCamera()
        initializeCamera()
    }
    
    override public var shouldAutorotate: Bool {
        return false
    }

    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }

    func configDocument() {
        if captureType == CAPTURETYPE.IDDOCUMENT {
            config.captureDocuments = DOCUMENT_MODE_IDDOCUMENT as? [IDVDOCDocument]
            if let numberOfPages = configuration[EXTRA_CAPTURE_NB_SIDES] as? Int { totalSide = numberOfPages } else {
                totalSide = 2
            }
            PLog(self, "<<<<<ID Card>>>>>")
        } else if (captureType == CAPTURETYPE.PASSPORT) {
             if let numberOfPages = configuration[EXTRA_CAPTURE_NB_SIDES] as? Int { totalSide = numberOfPages } else {
                totalSide = 1
            }
            config.captureDocuments = DOCUMENT_MODE_PASSPORT as? [IDVDOCDocument]
            PLog(self, "<<<<Passport>>>>")
        } else if (captureType == CAPTURETYPE.ICAO) {
            if let numberOfPages = configuration[EXTRA_CAPTURE_NB_SIDES] as? Int { totalSide = numberOfPages } else {
                totalSide = 1
            }
            config.captureDocuments = DOCUMENT_MODE_ICAO as? [IDVDOCDocument]
            PLog(self, "<<<<ICAO>>>>")
        } else if (captureType == CAPTURETYPE.CUSTOM) {
            if let numberOfPages = configuration[EXTRA_CAPTURE_NB_SIDES] as? Int { totalSide = numberOfPages } else {
                totalSide = 1
            }
            let customSize = configuration[EXTRA_DOC_SIZE] as? Dictionary<String,Any>
            PLog(self, customSize.debugDescription)
            
            var customWidth: Float = 0.0
            if let width = customSize?["Width"] as? NSNumber {
                customWidth = width.floatValue
            }
            
            var customHeight: Float = 0.0
            if let height = customSize?["Height"] as? NSNumber {
                customHeight = height.floatValue
            }
            PLog(self, "<<<<CUSTOM>>>> w:\(customWidth) h:\(customHeight)")
            config.captureDocuments = [IDVDOCDocument.init(width: customWidth, andHeight: customHeight)]
       }
    }
    
    func prepareCaptor(_config: Dictionary<String, Any>) {
        configuration = _config
        
        if let mode = configuration[EXTRA_CAPTURE_MODE] as? String {
            switch mode {
            case "PASSPORT":
                captureType = CAPTURETYPE.PASSPORT
            case "IDDOCUMENT":
                captureType = CAPTURETYPE.IDDOCUMENT
            case "ICAO":
                captureType = CAPTURETYPE.ICAO
            default:
                captureType = CAPTURETYPE.CUSTOM
            }
        }
         
        if let mode = configuration[EXTRA_CAPTURE_DETECTION_MODE] as? String {
            switch mode {
            case "MachineLearning":
                configuration[EXTRA_CAPTURE_DETECTION_MODE] = IDVDOCDetectionMode.DetectionMachineLearning
            case "ImageProcessing":
                configuration[EXTRA_CAPTURE_DETECTION_MODE] = IDVDOCDetectionMode.DetectionImageProcessing
            default:
                configuration[EXTRA_CAPTURE_DETECTION_MODE] = IDVDOCDetectionMode.DetectionDisabled
            }
        }
        
        if let glare = configuration[EXTRA_CAPTURE_QUALITY_CHECK_GLARE] as? String {
            switch glare {
            case "White":
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_GLARE] = GlareMode.white
            case "Color":
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_GLARE] = GlareMode.color
            default:
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_GLARE] = GlareMode.disabled
            }
        }
        
        if let blur = configuration[EXTRA_CAPTURE_QUALITY_CHECK_BLUR] as? String {
            switch blur {
            case "Strict":
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_BLUR] = BlurMode.strict
            case "Relaxed":
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_BLUR] = BlurMode.relaxed
            default:
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_BLUR] = BlurMode.disabled
            }
        }
        
        if let photocopy = configuration[EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY] as? String {
            switch photocopy {
            case "BlackAndWhite":
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY] = PhotocopyMode.blackAndWhite
            default:
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY] = PhotocopyMode.disabled
            }
        }
 
        if let darkness = configuration[EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS] as? String {
            switch darkness {
            case "Relaxed":
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS] = DarknessMode.relaxed
            default:
                configuration[EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS] = DarknessMode.disabled
            }
        }
        
        if let quality = configuration[EXTRA_CAPTURE_JPEG_COMPRESSION] as? Float {
            jpegQuality = quality / 100.0
        }
        PLog(self, "jpegQuality : \(jpegQuality)")
    }
    
    private func setupConfirmBox(){
        confirmBox.layer.borderWidth = 1
        confirmBox.layer.borderColor = UIColor.white.cgColor
    }
    
    private func showHideFlashMessage(hide: Bool) {
        lblFlashMessage.isHidden = hide
    }
    
    func configCamera() {
        if let objLicense = configuration[EXTRA_IDV_LICENSE] as? String { license = objLicense }
        if let objOverLay = configuration[EXTRA_CAPTURE_OVERLAY] as? Bool { isOverLay = objOverLay }
        PLog(self, "isOverLay : \(isOverLay)")
        
        if let objDetectionMode = configuration[EXTRA_CAPTURE_DETECTION_MODE] as? IDVDOCDetectionMode {
            switch objDetectionMode {
            case .DetectionImageProcessing:
                PLog(self, "DetectionImageProcessing")
                config.detectionMode = .DetectionImageProcessing
                isMachineLearning = false
            case .DetectionMachineLearning:
                PLog(self, "DetectionMachineLearning")
                config.detectionMode = .DetectionMachineLearning
                isMachineLearning = true
            default:
                config.detectionMode = .DetectionDisabled
                isMachineLearning = false
            }
        } else {
            PLog(self, "Default DetectionImageProcessing")
            config.detectionMode = .DetectionImageProcessing
        }
        if let glareCheck = configuration[EXTRA_CAPTURE_QUALITY_CHECK_GLARE] as? GlareMode {
            PLog(self, "glareCheck :\(glareCheck.rawValue)")
            config.qualityChecks.glareDetectionMode = GlareMode(rawValue: glareCheck.rawValue)!
        }
        if let photocopy = configuration[EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY] as? PhotocopyMode {
            PLog(self, "photocopy :\(photocopy.rawValue)")
            config.qualityChecks.photocopyDetectionMode = PhotocopyMode(rawValue: photocopy.rawValue)!
        }
        if let blurMode = configuration[EXTRA_CAPTURE_QUALITY_CHECK_BLUR] as? BlurMode {
            PLog(self, "blurMode :\(blurMode.rawValue)")
            config.qualityChecks.blurDetectionMode = BlurMode(rawValue: blurMode.rawValue)!
        }
        if let darkNessMode = configuration[EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS] as? DarknessMode {
            PLog(self, "darkNessMode :\(darkNessMode.rawValue)")
            config.qualityChecks.darknessDetectionMode = DarknessMode(rawValue: darkNessMode.rawValue)!
        }
    }
    
    func initializeCamera() {
        PLog(self, "initialize Camera")
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: // The user has previously granted access to the camera.
            init_sdk()
        case .notDetermined: // The user has not yet been asked for camera access.
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    self.init_sdk()
                }
            }
        case .denied: // The user has previously denied access.
            return
        case .restricted: // The user can't grant access due to restrictions.
            break
        @unknown default:
            return
        }
    }
    
    func init_sdk() {
        PLog(self, "Init SDK")
        
        sdk.`init`(license, view: camera, onInit: { completed, error in
            if (completed) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {  [weak self] in
                    self?.drawOverlay.draw(DocumentPlaceholder._TD1)
                    self?.drawOverlay.isHidden = !(self?.isOverLay ?? false)
                    self?.drawOverlay.setCameraFrame((self?.camera.frame)!)
                    self?.drawOverlay.setNeedsDisplay()
                    self?.sdk.start(self?.config, withBlock: self)
                }
            } else {
                PLog(self, "Error onInit: \(error)")
            }
        })
    }

    @IBAction func onBackButtonClick() {
        PLog(self, "back pressed")
        let pluginResult = CDVPluginResult(
            status: CDVCommandStatus_ERROR,
            messageAs: "Failure: code=\(FailureCompanion.init().REASON_DOCUMENT_CAPTURE_ABORT) msg=action cancelled by user"
        )
        self.m_commandDelegate.send(pluginResult, callbackId:self.m_callbackId)
        sdk.stop()
        sdk.finish()
        self.dismiss(animated: true)
    }

    @IBAction func onCancelButtonClick() {
        PLog(self, "cancel pressed")
        camera.isHidden = false
        confirmBox.isHidden = true //stackView.isHidden = true
        captureImageView.isHidden = true
        self.drawOverlay.isHidden = false
        self.showHideFlashMessage(hide: false)
        self.imageArray.remove(at: currentSide-1) // remove image data from buffer when user cancels
        init_sdk()
    }
    
    @IBAction func onOKButtonClick() {
        PLog(self, "ok pressed: user accepted page= \(currentSide)")
        
        if imageArray.count > 0 {
            m_pageDict["page\(currentSide)"] = imageArray[currentSide-1]?.base64EncodedString()
        }
        
        if currentSide < totalSide {
            camera.isHidden = false
            currentSide += 1
            init_sdk()
            confirmBox.isHidden = true //stackView.isHidden = true
            self.lblFlashMessage.text = "Capturing back side."
            self.showHideFlashMessage(hide: false)
            captureImageView.isHidden = true
        } else {
            var pages = Dictionary<String,Any>()
            pages["pages"] = m_pageDict
            let pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: pages
            )
            self.m_commandDelegate.send(pluginResult, callbackId:self.m_callbackId)
            self.dismiss(animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            UINavigationController.attemptRotationToDeviceOrientation()
        }
        sdk.finish()
    }

    func onProcessedFrame(_ partial: IDVDOCCaptureResult!) {
        var qualityText = ""
        if !partial.qualityCheckResults.all {
            if partial.qualityCheckResults.glare {
                qualityText = "Glare detected"
            } else if partial.qualityCheckResults.blur {
                qualityText = "Blur detected"
            } else if partial.qualityCheckResults.darkness {
                qualityText = "Shadow detected"
            }else if partial.qualityCheckResults.photocopy {
                qualityText = "Photocopy detected"
            } else if partial.qualityCheckResults.contrast {
                if(!self.isMachineLearning){
                    qualityText = "Low contrast"
                }
            }
            else {
                qualityText = ""
            }
        }

        DispatchQueue.main.async { [weak self] in
         //   self?.drawOverlay.drawLinePoints(partial.quadrangle.topLeft, partial.quadrangle.topRight, partial.quadrangle.bottomRight, partial.quadrangle.bottomLeft)
            self?.lblFlashMessage.text = qualityText
            self?.showHideFlashMessage(hide: qualityText == "")
        }
    }

    func onSuccess(_ result: IDVDOCCaptureResult!) {
        if let croppedImage = result.cropFrame {
            PLog(self, "Before compression : \(croppedImage.count / 1024) KB")
            DispatchQueue.main.async { [weak self] in
                if let image = UIImage.init(data: croppedImage) {
                    self?.captureImageView.image = image
                    self?.captureImageView.isHidden = false
                    self?.captureImageView.contentMode = .scaleAspectFit
                    self?.drawOverlay.setCameraFrame((self?.captureImageView.frame)!)
                    self?.drawOverlay.isHidden = true
                    self?.sdk.finish()
                    self?.camera.isHidden = true
                    self?.confirmBox.isHidden = false //self?.stackView.isHidden = false
                    self?.checkImageSize(capturedImage: image)
                    self?.lblFlashMessage.text = ""
                    self?.showHideFlashMessage(hide: true)
                }
            }
        }
    }
    
    func checkImageSize(capturedImage : UIImage) {
        if let imageData = capturedImage.jpegData(compressionQuality: CGFloat(jpegQuality)) {
            PLog(self, "After compression : \(imageData.count / 1024) KB")
            let data = imageData
            self.imageArray.insert(data, at: self.currentSide-1)
        }
    }
}
   
