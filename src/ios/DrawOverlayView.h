/**
 * DrawOverlayView.h
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

#ifndef DrawOverlayView_h
#define DrawOverlayView_h
#import <UIKit/UIKit.h>

typedef NS_ENUM( NSInteger, DocumentPlaceholder ) {
    DocumentPlaceholder_TD1 = 0,
    DocumentPlaceholder_TD3 = 1,
    DocumentPlaceholder_TD4 = 2
};

@interface DrawOverlayView : UIView

- (void)setCameraFrame:(CGRect)rect;
- (void)drawDocumentPlaceholder:(DocumentPlaceholder)placeholder;
- (void)drawLinePoints:(CGPoint)topLeft :(CGPoint)topRight :(CGPoint)bottomRight :(CGPoint)bottomLeft;
- (void)clear;

@end

#endif
