/**
 * DrawOverlayView.m
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

#import "DrawOverlayView.h"
#import <AVFoundation/AVFoundation.h>

@implementation DrawOverlayView {
    BOOL needToDrawDocumentPlaceholder;
    BOOL needToDrawLineWithPoints;
    
    NSArray *normalizedPoints;
    
    DocumentPlaceholder mPlaceholder;
}

- (void)setCameraFrame:(CGRect) rect {
    self.frame = rect;

    // Removing all constraints relative to the view
    UIView *superview = self.superview;
    while (superview != nil) {
        for (NSLayoutConstraint *c in superview.constraints) {
            if (c.firstItem == self || c.secondItem == self) {
                [superview removeConstraint:c];
            }
        }
        superview = superview.superview;
    }
    [self removeConstraints: self.constraints];

    // Set new constraints automatically
    [self setTranslatesAutoresizingMaskIntoConstraints: YES];
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();

    if (needToDrawDocumentPlaceholder) {
    
        CGFloat viewH  = self.bounds.size.height;
        CGFloat viewW  = self.bounds.size.width;
        CGFloat viewAR = viewW / viewH;
        CGFloat docAR;
        CGFloat shortSide;
        
        CGFloat min       = viewW < viewH ? viewW : viewH;
        if (mPlaceholder == DocumentPlaceholder_TD4) {
            docAR  = (210.0/297.0);
            shortSide = min * 90/100;
        } else {
            docAR  = mPlaceholder == DocumentPlaceholder_TD3 ? 1.42 : 1.58;
            shortSide = min * 80/100;
        }
        CGFloat longSide  = viewAR > 1.0 ? shortSide * docAR : shortSide / docAR;

        CGFloat w = viewW < viewH ? shortSide : longSide;
        CGFloat h = viewW < viewH ? longSide : shortSide;

        CGRect quadrangle = CGRectMake((self.bounds.size.width - w) / 2, (self.bounds.size.height - h) / 2, w, h);

        CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
        CGContextSetStrokeColorWithColor(context, UIColor.whiteColor.CGColor);
        CGContextStrokeRectWithWidth(context, quadrangle, 4);
        CGContextFillRect(context, quadrangle);
        CGContextStrokeRect(context, quadrangle);
    }
    
    if (needToDrawLineWithPoints) {
        
        CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
        CGContextSetLineWidth(context, 4);
        CGPathRef linesPath = [self getLinesPath];
        CGContextAddPath(context, linesPath);
        CGContextFillPath(context);
        CGContextSetStrokeColorWithColor(context, UIColor.blueColor.CGColor);
        CGContextAddPath(context, linesPath);
        CGContextStrokePath(context);
        CGPathRelease(linesPath);
    }
}


- (CGPathRef) getLinesPath {

    CGPoint topLeft = [normalizedPoints[0] CGPointValue];
    CGPoint topRight = [normalizedPoints[1] CGPointValue];
    CGPoint bottomRight = [normalizedPoints[2] CGPointValue];
    CGPoint bottomLeft = [normalizedPoints[3] CGPointValue];
    
    CGMutablePathRef linesPath = CGPathCreateMutable();
    CGPathMoveToPoint(linesPath, NULL, topLeft.x, topLeft.y);
    CGPathAddLineToPoint(linesPath, NULL,topRight.x, topRight.y);
    CGPathAddLineToPoint(linesPath, NULL, bottomRight.x, bottomRight.y);
    CGPathAddLineToPoint(linesPath, NULL, bottomLeft.x, bottomLeft.y);
    CGPathAddLineToPoint(linesPath, NULL, topLeft.x, topLeft.y);

    CGPathCloseSubpath(linesPath);
    
    return linesPath;
}


- (void)drawDocumentPlaceholder:(DocumentPlaceholder) placeholder {
    needToDrawDocumentPlaceholder = YES;
    mPlaceholder = placeholder;
}


- (void)drawLinePoints:(CGPoint)topLeftPoint :(CGPoint)topRightPoint :(CGPoint)bottomRightPoint :(CGPoint)bottomLeftPoint {
    normalizedPoints = [self normalizePoints:topLeftPoint :topRightPoint :bottomRightPoint :bottomLeftPoint];
    needToDrawLineWithPoints = YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
    });
}


- (NSArray *)normalizePoints:(CGPoint)topLeftPoint :(CGPoint)topRightPoint :(CGPoint)bottomRightPoint :(CGPoint)bottomLeftPoint {
    
    CGFloat viewWidth = self.bounds.size.width;
    CGFloat viewHeight = self.bounds.size.height;
    
    NSArray *points = @[[NSValue valueWithCGPoint: CGPointMake(topLeftPoint.x * viewWidth, topLeftPoint.y * viewHeight)],
                        [NSValue valueWithCGPoint: CGPointMake(topRightPoint.x * viewWidth,  topRightPoint.y * viewHeight)],
                        [NSValue valueWithCGPoint: CGPointMake(bottomRightPoint.x * viewWidth, bottomRightPoint.y * viewHeight)],
                        [NSValue valueWithCGPoint: CGPointMake(bottomLeftPoint.x * viewWidth, bottomLeftPoint.y * viewHeight)]];

    return points;
}


- (void)clear {
    [self clearPlaceholder];
    [self clearPoints];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
    });
}

- (void)clearPlaceholder {
    needToDrawDocumentPlaceholder = NO;
}


- (void)clearPoints {
    needToDrawLineWithPoints = NO;
}

@end
