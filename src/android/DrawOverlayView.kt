/**
 * DrawOverlayView.kt
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi

import android.content.Context
import android.graphics.Canvas
import android.graphics.CornerPathEffect
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View

import java.util.Objects
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

class DrawOverlayView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0) : View(context, attrs, defStyleAttr, defStyleRes) {
    private val mPath: Path

    private val mDocumentPaint: Paint
    private val mBorderPaint: Paint
    private val mFillPaint: Paint

    private val mNeedToDrawDocument: AtomicBoolean
    private val mNeedToDrawPoints: AtomicBoolean

    private val mWidth: AtomicInteger
    private val mHeight: AtomicInteger
    private val mQuadrangle: Rect

    private var mDrawDocumentPlaceholder = false

    // To customize
    // Document
    private val mDocumentColor = -0x1
    // Corners
    // -- common
    private val mStroke = 10.0f
    private val mPathRadius = 10
    // -- outer
    private val mBorderColor = -0xffff01
    private val mPadding = 20
    // -- inner
    private val mFillColor = -0x6c623101
    private val mAlpha = 100

    init {

        mPath = Path()

        mDocumentPaint = Paint()
        mDocumentPaint.style = Paint.Style.STROKE
        mDocumentPaint.strokeWidth = mStroke
        mDocumentPaint.strokeJoin = Paint.Join.ROUND
        mDocumentPaint.strokeCap = Paint.Cap.ROUND
        mDocumentPaint.pathEffect = CornerPathEffect(mPathRadius.toFloat())
        mDocumentPaint.color = mDocumentColor
        mDocumentPaint.isAntiAlias = true
        mDocumentPaint.isDither = true

        mBorderPaint = Paint()
        mBorderPaint.style = Paint.Style.STROKE
        mBorderPaint.strokeWidth = mStroke
        mBorderPaint.strokeJoin = Paint.Join.ROUND
        mBorderPaint.strokeCap = Paint.Cap.ROUND
        mBorderPaint.pathEffect = CornerPathEffect(mPathRadius.toFloat())
        mBorderPaint.color = mBorderColor
        mBorderPaint.isAntiAlias = true
        mBorderPaint.isDither = true

        mFillPaint = Paint()
        mFillPaint.style = Paint.Style.FILL
        mFillPaint.color = mFillColor
        mFillPaint.alpha = mAlpha
        mFillPaint.isAntiAlias = true
        mFillPaint.isDither = true

        mNeedToDrawDocument = AtomicBoolean(false)
        mNeedToDrawPoints = AtomicBoolean(false)

        mWidth = AtomicInteger(0)
        mHeight = AtomicInteger(0)

        mQuadrangle = Rect()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        mWidth.set(widthSize)
        mHeight.set(heightSize)

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (mDrawDocumentPlaceholder) {
            drawDocument()
            mDrawDocumentPlaceholder = false
        }
        if (mNeedToDrawDocument.get()) {
            canvas.drawRect(mQuadrangle, mDocumentPaint)
        }
        if (mNeedToDrawPoints.get()) {
            if (!mPath.isEmpty) {
                canvas.drawPath(mPath, mFillPaint)
                canvas.drawPath(mPath, mBorderPaint)
            }
            mNeedToDrawPoints.set(false)
        }
    }

    fun drawDocumentPlaceholder(drawDocumentPlaceholder: Boolean) {
        mDrawDocumentPlaceholder = drawDocumentPlaceholder
    }

    private fun drawDocument() {

        val viewW = mWidth.toFloat().toDouble()
        val viewH = mHeight.toFloat().toDouble()
        val viewAR = viewW / viewH
        val docAR = 1.58 //(idSelected) ? 1.58 : 1.42;
        val min = if (viewW < viewH) viewW else viewH
        val shortSide = min * 80 / 100
        val computedSide = if (viewAR > 1.0) shortSide * docAR else shortSide / docAR

        val w = if (viewW < viewH) shortSide else computedSide
        val h = if (viewW < viewH) computedSide else shortSide

        val offsetX = ((viewW - w) / 2).toInt()
        val offsetY = ((viewH - h) / 2).toInt()
        mQuadrangle.set(offsetX, offsetY, offsetX + w.toInt(), offsetY + h.toInt())

        mNeedToDrawDocument.set(true)

        post { invalidate() }
    }

    fun drawPoints(
            topLeft: PointF, topRight: PointF,
            bottomRight: PointF, bottomLeft: PointF
    ) {
        Objects.requireNonNull(topLeft, "topLeft must have a value")
        Objects.requireNonNull(topRight, "topRight must have a value")
        Objects.requireNonNull(bottomRight, "bottomRight must have a value")
        Objects.requireNonNull(bottomLeft, "bottomLeft must have a value")

        if (isEmpty(topLeft, topRight, bottomRight, bottomLeft)) {
            cleanPath(mPath)
            return
        }

        val normalized = normalize(topLeft, topRight, bottomRight, bottomLeft)
        val withPadding = padding(normalized[0], normalized[1], normalized[2], normalized[3])

        mNeedToDrawPoints.set(true)
        post {
            mPath.rewind()
            mPath.moveTo(withPadding[0].x, withPadding[0].y) // topLeft
            mPath.lineTo(withPadding[1].x, withPadding[1].y) // topRight
            mPath.lineTo(withPadding[3].x, withPadding[3].y) // bottomRight
            mPath.lineTo(withPadding[2].x, withPadding[2].y) // bottomLeft
            mPath.lineTo(withPadding[0].x, withPadding[0].y) // topLeft
            mPath.close()
            invalidate()
        }
    }

    fun clear() {
        clearDocument()
        clearPoints()
    }

    fun clearDocument() {
        mNeedToDrawDocument.set(true)
        mQuadrangle.set(0, 0, 0, 0)
        post { invalidate() }
    }

    fun clearPoints() {
        mNeedToDrawPoints.set(true)
        cleanPath(mPath)
    }

    private fun normalize(
            topLeft: PointF, topRight: PointF,
            bottomLeft: PointF, bottomRight: PointF
    ): Array<PointF> {
        val nTopLeftX = topLeft.x * mWidth.get()
        val nTopLeftY = topLeft.y * mHeight.get()

        val nTopRightX = topRight.x * mWidth.get()
        val nTopRightY = topRight.y * mHeight.get()

        val nBottomRightX = bottomRight.x * mWidth.get()
        val nBottomRightY = bottomRight.y * mHeight.get()

        val nBottomLeftX = bottomLeft.x * mWidth.get()
        val nBottomLeftY = bottomLeft.y * mHeight.get()

        val fTopLeft = PointF(nTopLeftX, nTopLeftY)
        val fTopRight = PointF(nTopRightX, nTopRightY)
        val fBottomRight = PointF(nBottomRightX, nBottomRightY)
        val fBottomLeft = PointF(nBottomLeftX, nBottomLeftY)

        return arrayOf(fTopLeft, fTopRight, fBottomRight, fBottomLeft)
    }

    private fun padding(
            topLeft: PointF, topRight: PointF,
            bottomRight: PointF, bottomLeft: PointF
    ): Array<PointF> {
        val pTopLeftX = topLeft.x - mPadding
        val pTopLeftY = topLeft.y - mPadding

        val pTopRightX = topRight.x + mPadding
        val pTopRightY = topRight.y - mPadding

        val pBottomRightX = bottomRight.x + mPadding
        val pBottomRightY = bottomRight.y + mPadding

        val pBottomLeftX = bottomLeft.x - mPadding
        val pBottomLeftY = bottomLeft.y + mPadding

        val fTopLeft = PointF(pTopLeftX, pTopLeftY)
        val fTopRight = PointF(pTopRightX, pTopRightY)
        val fBottomRight = PointF(pBottomRightX, pBottomRightY)
        val fBottomLeft = PointF(pBottomLeftX, pBottomLeftY)

        return arrayOf(fTopLeft, fTopRight, fBottomRight, fBottomLeft)
    }

    private fun isEmpty(
            topLeft: PointF, topRight: PointF,
            bottomRight: PointF, bottomLeft: PointF
    ): Boolean {
        return topLeft.x == 0f && topLeft.y == 0f &&
                topRight.x == 0f && topRight.y == 0f &&
                bottomRight.x == 0.0f && bottomRight.y == 0.0f &&
                bottomLeft.x == 0f && bottomLeft.y == 0.0f
    }

    private fun cleanPath(path: Path) {
        post {
            path.rewind()
            invalidate()
        }
    }
}
