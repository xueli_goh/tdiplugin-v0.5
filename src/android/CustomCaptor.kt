/**
 * CustomCaptor.kt
 * Custom captor using IDV SDK.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.view.TextureView
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.gemalto.tdi.Failure
import com.thalesgroup.idv.sdk.doc.api.*
import org.apache.cordova.CallbackContext
import org.apache.cordova.PluginResult
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import applicationId.R

class CustomCaptor: AppCompatActivity() {
    companion object {
        private val TAG = "${CustomCaptor::class}"
        const val EXTRA_CAPTURE_MODE = "EXTRA_CAPTURE_MODE"
        const val EXTRA_CAPTURE_NB_SIDES = "EXTRA_CAPTURE_NB_SIDES"
        const val EXTRA_CAPTURE_OVERLAY = "EXTRA_CAPTURE_OVERLAY"
        const val EXTRA_IDV_LICENSE = "EXTRA_IDV_LICENSE"
        const val EXTRA_CAPTURE_DETECTION_MODE = "EXTRA_CAPTURE_DETECTION_MODE"
        const val EXTRA_CAPTURE_QUALITY_CHECK_BLUR = "EXTRA_CAPTURE_QUALITY_CHECK_BLUR"
        const val EXTRA_CAPTURE_QUALITY_CHECK_GLARE = "EXTRA_CAPTURE_QUALITY_CHECK_GLARE"
        const val EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS = "EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS"
        const val EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY = "EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY"
        const val EXTRA_DOC_SIZE = "EXTRA_DOC_SIZE"
        const val EXTRA_CAPTURE_JPEG_COMPRESSION = "EXTRA_CAPTURE_JPEG_COMPRESSION"

        const val PASSPORT = 0
        const val ID_DOCUMENT = 1
        const val ICAO = 2
        const val CUSTOM = 3

        var mCallbackCtx: CallbackContext? = null
        private var mNumSides = 1
        private var mIdvSdkLicense = ""
    }

    private var mSDK: CaptureSDK? = null
    private lateinit var captureMode: MutableList<Document>
    private val mConfig = Configuration()
    private var currentSide = 1
    private var detection_mode = Configuration.MachineLearning

    private var blurDetectionMode = Configuration.Strict
    private var glareDetectionMode = Configuration.White
    private var darknessDetectionMode = Configuration.Relaxed
    private var photocopyDetectionMode = Configuration.BlackAndWhite

    private var idv_error = Failure.REASON_DOCUMENT_CAPTURE_ABORT
    private var numberOfSides = 1
    private var input = mutableMapOf<String,String>()
    private var isOverlay: Boolean = true
    private var isFeedbackOn: Boolean = false
    private var jpegCompression: Int = 80 // Percent

  private val view by lazy { findViewById<TextureView>(R.id.camera)}
  private val mDrawOverlayView by lazy {findViewById<DrawOverlayView>(R.id.draw_overlay)}
  private val qualityLabel by lazy {findViewById<TextView>(R.id.qualityLabel)}
  private val resultView by lazy {findViewById<LinearLayout>(R.id.resultView)}
  private val cropResultImage by lazy {findViewById<ImageView>(R.id.cropResultImage)}
  private val buttonLayout by lazy {findViewById<LinearLayout>(R.id.buttonLayout)}
  private val backgroundResult by lazy {findViewById<LinearLayout>(R.id.backgroundResult)}
  private val cancelButton by lazy {findViewById<Button>(R.id.cancelButton)}
  private val okButton by lazy {findViewById<Button>(R.id.okButton)}
  private val nextSideImage by lazy {findViewById<ImageView>(R.id.nextSideImage)}
  private val nextSideText by lazy {findViewById<TextView>(R.id.nextSideText)}

    fun setPluginCallback(_callbackContext: CallbackContext) {
        mCallbackCtx = _callbackContext
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.custom_captor)
        mSDK = CaptureSDK()

        this.captureMode = when (intent.getIntExtra(EXTRA_CAPTURE_MODE, CUSTOM)) {
            PASSPORT -> Document.DocumentModePassport
            ID_DOCUMENT -> Document.DocumentModeIdDocument
            ICAO -> Document.DocumentModeICAO
            else -> {
                // Custom document
                val docSize: FloatArray = intent.getFloatArrayExtra(EXTRA_DOC_SIZE) as FloatArray
                PLog.d(TAG,"Custom doc size: w:${docSize[0]} h:${docSize[1]}")
                mutableListOf(Document(docSize[0], docSize[1]))
            }
        }

        this.isOverlay = intent.getBooleanExtra(EXTRA_CAPTURE_OVERLAY, true)
        this.numberOfSides = intent.getIntExtra(EXTRA_CAPTURE_NB_SIDES, mNumSides)
        this.detection_mode = intent.getIntExtra(EXTRA_CAPTURE_DETECTION_MODE, Configuration.MachineLearning)
        this.blurDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_QUALITY_CHECK_BLUR, Configuration.Strict)
        this.glareDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_QUALITY_CHECK_GLARE, Configuration.White)
        this.darknessDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS, Configuration.Relaxed)
        this.photocopyDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY, Configuration.BlackAndWhite)
        this.jpegCompression = intent.getIntExtra(EXTRA_CAPTURE_JPEG_COMPRESSION, jpegCompression)

        PLog.d(TAG,"IDV DOC SDK: " + CaptureSDK.version + " Sides: " + numberOfSides + " JPEG: " + jpegCompression+ " edge: " + detection_mode + " blur: " + blurDetectionMode + " glare: " + glareDetectionMode + " dark: " + darknessDetectionMode + " BW: " + photocopyDetectionMode)
    }

    override fun onBackPressed() {
        PLog.d(TAG,"onBackPressed: Cancelling current capture")
        val pluginResult = PluginResult(PluginResult.Status.ERROR,
            "Failure: code=${Failure.REASON_DOCUMENT_CAPTURE_ABORT} msg=${getError(Failure.REASON_DOCUMENT_CAPTURE_ABORT)}")
        mCallbackCtx?.sendPluginResult(pluginResult)
        super.onBackPressed()
    }

    private fun processError() {
        PLog.e(TAG,"processError: " + getError(this.idv_error) + ": " + Integer.toHexString(this.idv_error))
        mCallbackCtx?.error("Error: code=${Integer.toHexString(this.idv_error)} " +
                    "msg=${getError(this.idv_error)}")

        if (mSDK != null) {
            mSDK!!.finish()
        }
        finish()
    }


    override fun onDestroy() {
        if (mSDK != null) {
            mSDK!!.finish()
        }

        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        val license = if (mIdvSdkLicense.isNotEmpty()) mIdvSdkLicense else intent.getStringExtra(EXTRA_IDV_LICENSE)

        mSDK!!.init<TextureView>(license, view, { isCompleted, errorCode ->
            if (isCompleted) {
                mDrawOverlayView.drawDocumentPlaceholder(isOverlay)

                runOnUiThread {
                    mDrawOverlayView.requestLayout()
                    mDrawOverlayView.layoutParams = view.layoutParams
                    mDrawOverlayView.invalidate()
                }
                startSDK()
            } else {
                PLog.e(TAG, "Error on init: 0x" + errorCode.toString(16) + " : " + getError(errorCode))
                this.idv_error = errorCode
                processError()
            }
        })
    }

    override fun onPause() {
        super.onPause()

        if (mSDK != null) {
            mSDK!!.finish()
        }
    }

    private fun startSDK() {
        mConfig.captureDocuments = this.captureMode
        mConfig.detectionMode = this.detection_mode
        mConfig.qualityChecks.blurDetectionMode = this.blurDetectionMode
        mConfig.qualityChecks.glareDetectionMode = this.glareDetectionMode
        mConfig.qualityChecks.darknessDetectionMode = this.darknessDetectionMode
        mConfig.qualityChecks.photocopyDetectionMode = this.photocopyDetectionMode

        mSDK!!.start(mConfig, object : CaptureCallback.StartCallback {
            override fun onProcessedFrame(partial: CaptureResult) {
                if (!partial.qualityCheckResults.all && !isFeedbackOn) {
                    val qualityText = when {
                        partial.qualityCheckResults.glare -> {
                            resources.getString(R.string.idv_doc_glare)
                        }
                        partial.qualityCheckResults.blur -> {
                            resources.getString(R.string.idv_doc_blur)
                        }
                        partial.qualityCheckResults.darkness ->
                        {
                            resources.getString(R.string.idv_doc_shadows)
                        }
                        //Add for 4.1
                        partial.qualityCheckResults.noFocused ->
                        {
                            resources.getString(R.string.idv_doc_noty_focus)
                        }
                        partial.qualityCheckResults.photocopy ->  resources.getString(R.string.idv_doc_photocopy)
                        partial.qualityCheckResults.contrast ->  {
                            resources.getString(R.string.idv_doc_contrast)
                        }
                        else -> ""
                    }
                    runOnUiThread {
                        if (qualityText != "") {
                            resultView.visibility = View.VISIBLE
                            qualityLabel.text = qualityText
                            isFeedbackOn = true
                        } else {
                            resultView.visibility = View.GONE
                        }

                        Handler().postDelayed( { // Avoid Feedback to be all time on screen
                            resultView.visibility = View.GONE
                            isFeedbackOn = false
                        }, 1500)
                    }
                } else {
                    runOnUiThread {
                        resultView.visibility = View.GONE
                    }
                }
            }

            override fun onSuccess(result: CaptureResult) {
                PLog.d(TAG,"Success")

                //Try to compress to JPEG because SDK 4.0.1 return a JPEG > 1MB!!!
                try {
                    var byteArray:ByteArray = result.cropFrame
                    var bitmap = BitmapFactory.decodeByteArray(byteArray, 0, result.cropFrame.size)

                    PLog.d(TAG,"Before compression = ${result.cropFrame.size / 1024} KB")

                    try {
                        val stream = ByteArrayOutputStream()
                        bitmap?.compress(Bitmap.CompressFormat.JPEG, jpegCompression, stream)
                        byteArray = stream.toByteArray()
                        stream.close()
                        bitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.size)
                        PLog.d(TAG,"After compression = ${byteArray.size / 1024} KB")
                    } catch (e: Exception) {
                    }

                    runOnUiThread {
                        cropResultImage.setImageBitmap(bitmap)
                        cropResultImage.visibility = View.VISIBLE
                        buttonLayout.visibility = View.VISIBLE
                        backgroundResult.setBackgroundColor(ContextCompat.getColor(this@CustomCaptor,R.color.idv_doc_result_background))
                        backgroundResult.visibility = View.VISIBLE
                        resultView.visibility = View.GONE
                    }

                    cancelButton.setOnClickListener {
                        runOnUiThread {
                            PLog.d(TAG, "User reject the capture: $currentSide")
                            cropResultImage.visibility = View.GONE
                            buttonLayout.visibility = View.GONE
                            backgroundResult.visibility = View.GONE
                        }
                        mSDK!!.start(mConfig,this)
                    }

                    okButton.setOnClickListener {
                        PLog.d(TAG, "User accept the capture: $currentSide")
                        input["page$currentSide"] = Base64.encodeToString(byteArray, Base64.NO_WRAP)

                        if (currentSide++ >= numberOfSides) { //FINISH
                            PLog.d(TAG, "End of process for ${currentSide-1} sides")
                            mSDK!!.finish()

                            val jsonObject = JSONObject()
                            input.let { mapData ->
                                val tempJsonObj = JSONObject()
                                mapData.forEach { eachInput ->
                                    tempJsonObj.put(eachInput.key, eachInput.value)
                                }
                                jsonObject.accumulate("pages", tempJsonObj)
                            }

                            mCallbackCtx?.success(jsonObject)
                            runOnUiThread {
                                finish()
                            }
                        } else { //Next side
                            nextSideImage.visibility = View.VISIBLE
                            nextSideText.visibility = View.VISIBLE
                            cropResultImage.visibility = View.GONE
                            buttonLayout.visibility = View.GONE

                            PLog.d(TAG, "End of process for side:  $currentSide")
                            backgroundResult.setBackgroundColor(ContextCompat.getColor(this@CustomCaptor,R.color.idv_doc_result_background_transparency))

                            backgroundResult.setOnClickListener {
                                runOnUiThread {
                                    buttonLayout.visibility = View.GONE
                                    backgroundResult.visibility = View.GONE
                                    nextSideImage.visibility = View.GONE
                                    nextSideText.visibility = View.GONE
                                    PLog.d(TAG, "User Tap on screen for side:  $currentSide")
                                    mSDK!!.start(mConfig,this)
                                }
                            }
                        }
                    }
                } catch (e: Exception) {
                    runOnUiThread {
                        Handler().postDelayed({ // Avoid crash on S6 5.1.1
                            mSDK!!.start(mConfig, this)
                        }, 500)
                        PLog.e(TAG, "onSuccess() Exception: ${e.toString()}")
                    }
                }
            }
        })
    }

    fun getError(errorNum: Int): String? {
        when (errorNum) {
            0x0100, 0x0200, 0x0300 -> return getString(R.string.idv_doc_no_error)
            0x0002 -> return getString(R.string.idv_doc_error_lib_not_init)
            0x0003 -> return getString(R.string.idv_doc_error_lib_already_init)
            0x0004 -> return getString(R.string.idv_doc_error_init_progress)
            0x0005 -> return getString(R.string.idv_doc_error_lib_stopping)
            0x0006 -> return getString(R.string.idv_doc_error_mrz_not_found)
            0x0007 -> return getString(R.string.idv_doc_error_lib_finish)
            0x0008 -> return getString(R.string.idv_doc_error_lib_iqa)
            0x0009 -> return getString(R.string.idv_doc_error_init_engine)
            0x0010 -> return getString(R.string.idv_doc_error_init_ocr)
            0x0101 -> return getString(R.string.idv_doc_error_architecture_error)
            0x0102 -> return getString(R.string.idv_doc_error_os_error)
            0x0103 -> return getString(R.string.idv_doc_error_permission_denied)
            0x0201 -> return getString(R.string.idv_doc_error_license_empty)
            0x0202 -> return getString(R.string.idv_doc_error_license_expired)
            0x0203 -> return getString(R.string.idv_doc_error_feature)
            0x0204 -> return getString(R.string.idv_doc_error_license_format)
            0x0205 -> return getString(R.string.idv_doc_error_license_hash)
            0x0206 -> return getString(R.string.idv_doc_error_license_version)
            0x0207 -> return getString(R.string.idv_doc_error_license_invalid_feature_list)
            0x0208 -> return getString(R.string.idv_doc_error_license_invalid_expiration)
            0x0209 -> return getString(R.string.idv_doc_error_license_invalid_timestamp)
            0x0210 -> return getString(R.string.idv_doc_error_license_invalid_cache)
            Failure.REASON_DOCUMENT_CAPTURE_ABORT -> return getString(R.string.idv_warning_user_cancellation)
        }
        return getString(R.string.idv_doc_error_unknow_error)
    }
}
