/*
 *  Copyright (c) 2017 GEMALTO
 *  This computer program includes confidential and proprietary information of
 *  Gemalto and is a trade secret of Gemalto. All use, disclosure, and/or
 *  reproduction is prohibited unless authorized in writing by Gemalto.
 *  All Rights Reserved.
 */

#ifndef Dep_errorcodes_h
#define Dep_errorcodes_h

 /**
 * @brief  DEP error codes
 */
typedef enum DEP_ERROR_CODE {
    kDEPErrorCode_Ok = 0,
    kDEPErrorCode_Fail,
    kDEPErrorCode_UnknownError,
    kDEPErrorCode_InvalidParameter,
    kDEPErrorCode_InvalidOutputParameter,
    kDEPErrorCode_AllocateMemoryFail,  //5
    kDEPErrorCode_InvalidHeaderFrame,
    kDEPErrorCode_InvalidBodyFrame,
    kDEPErrorCode_InvalidResponseTag,
    kDEPErrorCode_InvalidResponseData,
    kDEPErrorCode_InvalidResponseHeaderVersion, //10
    kDEPErrorCode_InvalidMac,
    kDEPErrorCode_InvalidAlgorithmIdentifier,
    kDEPErrorCode_InvalidResponseHeader,
    kDEPErrorCode_InvalidResponseBody,
    kDEPErrorCode_InvalidModulus,  //15
    kDEPErrorCode_InvalidExponent,
    kDEPErrorCode_InvalidDataKey,
    kDEPErrorCode_InvalidTimestamp,
    kDEPErrorCode_InvalidSession,
    kDEPErrorCode_ExceedMaxBufferSize,  //20
    kDEPErrorCode_NotSupport,
    kDEPErrorCode_True,
    kDEPErrorCode_False,
    kDEPErrorCode_OpenSSL_Fail,
    kDEPErrorCode_Max,
}DEPErrorCode;



#endif