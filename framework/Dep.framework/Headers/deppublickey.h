/*
 *  Copyright (c) 2017 GEMALTO
 *  This computer program includes confidential and proprietary information of
 *  Gemalto and is a trade secret of Gemalto. All use, disclosure, and/or
 *  reproduction is prohibited unless authorized in writing by Gemalto.
 *  All Rights Reserved.
 */



#ifndef Dep_publickey_h
#define Dep_publickey_h

#include "dep/depdatatypes.h"
#include "dep/deperrorcodes.h"

 /**
 *  Type def structure of public key
 */


#if defined __cplusplus
extern "C" {
#endif

    /**
     * Allocate memory for the public key object.
     *
     * @param[in] modulus   The given public modulus
     * @param[in] exponent  The given public exponent
     * @param[out] publicKey The given pointer to output public key
     *
     * @return kDEPErrorCode_Ok  Successful.
     *          kDEPErrorCode_Fail execute OpenSSL operation fail.
     *          otherwise indicate other error type.
     */
    DEPErrorCode DEPPublicKey_secureMalloc(const DEPByteArray * const modulus,
                                           const DEPByteArray * const exponent,
                                           DEPPublicKey ** publicKey);
    
    /**
     * De-allocate memory for the public key object.
     *
     * @param[in,out] publickey The given public key to be freed.
     */
    void DEPPublicKey_secureFree(DEPPublicKey ** publickey);
 

#if defined __cplusplus
};
#endif

#endif
