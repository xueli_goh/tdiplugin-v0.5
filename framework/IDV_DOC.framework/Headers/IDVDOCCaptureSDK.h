//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVDOCCaptureSDK_h
#define IDVDOCCaptureSDK_h

#import <UIKit/UIKit.h>

#import "IDV_DOC/IDVDOCCaptureCallback.h"
#import "IDV_DOC/IDVDOCConfiguration.h"

/**
 * 
 * */
@interface IDVDOCCaptureSDK : NSObject

/**
Called when init is completed.
This method indicates whether SDK initialization is completed.
To see the complete list of error codes please go to documentation.
@param isCompleted  Boolean saying initialization was ok or not
@param errorCode Error description. To see the complete list of error codes please go to documentation.
*/
typedef void (^ onInitBlock)(BOOL isCompleted, int errorCode);

/**
Called when stop is completed.
This method indicates whether SDK stop process is completed.
To see the complete list of error codes please go to documentation.
@param errorCode Error description. To see the complete list of error codes please go to documentation.
*/
typedef void (^ onStopBlock)(int errorCode);

/**
 @return current SDK version as NSString
 */
+ (NSString*) version;

/**
 Initializes the sdk. Shows the camera but doesn't start the capture process.

 @param license NSString with a valid license.
 @param view UIView to show the camera preview.
 @param onInit Block called after initialization is done.
*/
- (void) init:(NSString*)license view:(UIView *)view onInit:(onInitBlock)onInit;


/**
 Starts the capture process

 @param configuration Configuration to initialize the SDK.
 @param callback Listener to be called when a frame is processed.
 @return An error code if can't start. To see the complete list of error codes please go to documentation.
*/
- (int) start:(IDVDOCConfiguration *)configuration withBlock: (id<IDVDOCStartCallback>)callback;


/**
 Stops the capture process

 @return An error code if can't stop. To see the complete list of error codes please go to documentation.
*/
- (int) stop;

/**
 Finish the SDK, releasing resources.

 @return An error code if can't finish. To see the complete list of error codes please go to documentation.
*/
- (int) finish;

/**
 Triggers a manual capture.
*/
- (void) triggerCapture;

/**
 Crops an image returned by StartCallback listener.

 @param jpegImageBuffer NSData image reference from the SDK.
 @param quadrangle IDVDOCQuadrangle reference indicating the crop coordinates.
 @param configuration IDVDOCConfiguration reference with a license.
 @return An IDVDOCCaptureResult reference.
*/
- (IDVDOCCaptureResult*) cropFrame: (NSData*)jpegImageBuffer withQuadrangle:(IDVDOCQuadrangle*)quadrangle configuration: (IDVDOCConfiguration *)configuration;

/**
 Process an arbitrary image.

 @param license NSString with a valid license.
 @param configuration IDVDOCConfiguration reference with a license.
 @param uiImage input image.
 @return An IDVDOCCaptureResult reference.
*/
- (IDVDOCCaptureResult*) processImage:(NSString*) license configuration: (IDVDOCConfiguration *)configuration image:(UIImage*)uiImage;

@end

#endif
