//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVDOCCaptureCallback_h
#define IDVDOCCaptureCallback_h

#import "IDV_DOC/IDVDOCCaptureResult.h"

/**
 SDK frame process delegate
*/
@protocol IDVDOCStartCallback <NSObject>

/**
 Callback for each frame processed without a valid detected document
 @param partial IDVDOCCaptureResult reference with partial results.
*/
- (void) onProcessedFrame:(IDVDOCCaptureResult *)partial;

/**
 Callback called once when a valid detected document
 @param result IDVDOCCaptureResult reference with results.
*/
- (void) onSuccess:(IDVDOCCaptureResult *)result;

@end

#endif
