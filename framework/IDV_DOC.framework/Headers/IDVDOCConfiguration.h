//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVDOCConfiguration_h
#define IDVDOCConfiguration_h

#import "IDV_DOC/IDVDOCDocument.h"
#import "IDV_DOC/IDVDOCQualityChecks.h"
#import <AVFoundation/AVFoundation.h>


typedef NS_ENUM(NSInteger, IDVDOCDetectionMode) {
    DetectionDisabled = -1,
    DetectionImageProcessing,
    DetectionMachineLearning
};

/**
 Object containing configuration info to initialize the SDK.
*/
@interface IDVDOCConfiguration : NSObject {
    NSArray<IDVDOCDocument*>    *captureDocuments;
    IDVDOCQualityChecks         *qualityChecks;
}

@property (nonatomic, readwrite) NSArray<IDVDOCDocument*>* captureDocuments;
@property (nonatomic, readwrite) IDVDOCDetectionMode detectionMode;
@property (nonatomic, readwrite) IDVDOCQualityChecks* qualityChecks;

@end

#endif
