//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, BlurMode) {
    BlurDisabled = -1,
    BlurRelaxed,
    BlurStrict
};

typedef NS_ENUM(NSInteger, PhotocopyMode)  {
    PhotocopyDisabled = -1,
    PhotocopyBlackAndWhite
};

typedef NS_ENUM(NSInteger, GlareMode) {
    GlareDisabled = -1,
    GlareWhite,
    GlareColor
};

typedef NS_ENUM(NSInteger, DarknessMode) {
    DarknessDisabled = -1,
    DarknessRelaxed
};

/**
 Object containing quality check configuration
*/
@interface IDVDOCQualityChecks : NSObject {
    GlareMode       glareDetectiontMode;
    BlurMode        blurDetectionMode;
    PhotocopyMode   photocopyDetectionMode;
    DarknessMode    darknessDetectionMode;
}


@property (nonatomic, readwrite) GlareMode      glareDetectionMode;
@property (nonatomic, readwrite) BlurMode       blurDetectionMode;
@property (nonatomic, readwrite) PhotocopyMode  photocopyDetectionMode;
@property (nonatomic, readwrite) DarknessMode   darknessDetectionMode;

@end

NS_ASSUME_NONNULL_END
