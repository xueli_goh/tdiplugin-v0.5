//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVDOCDocument_h
#define IDVDOCDocument_h

#import <Foundation/Foundation.h>

/**
 Object containing ID document representation
*/

@interface IDVDOCDocument : NSObject {
    @public float width;
    @public float height;
    @public bool isICAO;
    @public double aspectRatio;
}

/**
 Create an instance of a document

 @param p_width width of the document
 @param p_height height of the document
 @return a document instance
 */
-(id)initWithWidth:(float)p_width andHeight:(float)p_height;

/**
 Compare two documentsw

 @param p_document document to compare
 @return true if the documents are equal
 */
-(BOOL)isEqual:(IDVDOCDocument*)p_document;

@end

extern IDVDOCDocument* DOCUMENT_TD1;
extern IDVDOCDocument* DOCUMENT_TD2;
extern IDVDOCDocument* DOCUMENT_TD3;

extern NSArray *DOCUMENT_MODE_IDDOCUMENT;
extern NSArray *DOCUMENT_MODE_PASSPORT;
extern NSArray *DOCUMENT_MODE_ICAO;

#endif
