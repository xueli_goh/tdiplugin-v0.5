//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVDOCCaptureResult_h
#define IDVDOCCaptureResult_h

#import <AVFoundation/AVFoundation.h>

/**
 Object containing coordinates
*/
@interface IDVDOCQuadrangle : NSObject

@property (readwrite) CGPoint topLeft;
@property (readwrite) CGPoint topRight;
@property (readwrite) CGPoint bottomLeft;
@property (readwrite) CGPoint bottomRight;

- (BOOL) isEmpty;

@end

/**
 Object containing quality check info
*/
@interface IDVDOCQualityCheckResults : NSObject

@property (readwrite) bool all;
@property (readwrite) bool contrast;
@property (readwrite) bool glare;
@property (readwrite) bool darkness;
@property (readwrite) bool blur;
@property (readwrite) bool photocopy;
@property (readwrite) bool noFocused;

@end

/**
 Object containing process results
*/
@interface IDVDOCCaptureResult : NSObject

@property (readwrite, strong) NSData *fullFrame;
@property (readwrite, strong) NSData *cropFrame;
@property (readwrite, strong) IDVDOCQuadrangle *quadrangle;
@property (readwrite, strong) IDVDOCQualityCheckResults *qualityCheckResults;
@property (nonatomic) int responseCode;

@end

#endif
