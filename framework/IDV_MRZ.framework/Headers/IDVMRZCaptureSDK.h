//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVMRZCaptureSDK_h
#define IDVMRZCaptureSDK_h

#import <UIKit/UIKit.h>

#import "IDVMRZConfiguration.h"
#import "IDVMRZCaptureCallback.h"

/**
 * 
 * */
@interface IDVMRZCaptureSDK : NSObject

/**
Called when init is completed.
This method indicates whether SDK initialization is completed.
To see the complete list of error codes please go to documentation.
@param isCompleted  Boolean saying initialization was ok or not
@param errorCode Error description. To see the complete list of error codes please go to documentation.
*/
typedef void (^ onInitBlock)(BOOL isCompleted, int errorCode);

/**
 @return current SDK version as NSString
 */
+ (NSString*) version;

/**
 Initializes the sdk. Shows the camera but doesn't start the capture process.

 @param license NSString with a valid license.
 @param view UIView to show the camera preview.
 @param onInit Block called after initialization is done.
*/
- (void) init:(NSString*)license view:(UIView *)view onInit:(onInitBlock)onInit;

/**
 Starts the capture process

 @param configuration Configuration to initialize the SDK.
 @param callback Listener to be called when a frame is processed.
 @return An error code if can't start. To see the complete list of error codes please go to documentation.
*/
- (int) start:(IDVMRZConfiguration *)configuration withBlock:(id<IDVMRZCaptureCallback>)callback;

/**
 Stops the capture process

 @return An error code if can't stop. To see the complete list of error codes please go to documentation.
*/
- (int) stop;

/**
 Finish the SDK, releasing resources.

 @return An error code if can't finish. To see the complete list of error codes please go to documentation.
*/
- (int) finish;

/**
 Process a signed image.

 @param jpegImageBuffer NSData image reference from the SDK.
 @param configuration IDVMRZConfiguration reference.
 @return An IDVDOCCaptureResult reference.
*/
- (IDVMRZCaptureResult*) processFrame: (NSData*)jpegImageBuffer configuration: (IDVMRZConfiguration *)configuration;

@end

#endif
