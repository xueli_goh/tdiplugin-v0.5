//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVMRZCaptureResult_h
#define IDVMRZCaptureResult_h

#import <AVFoundation/AVFoundation.h>

/**
 Object containing coordinates
*/
@interface IDVMRZQuadrangle : NSObject

@property (readwrite) CGPoint topLeft;
@property (readwrite) CGPoint topRight;
@property (readwrite) CGPoint bottomLeft;
@property (readwrite) CGPoint bottomRight;

- (BOOL) isEmpty;
@end

/**
 Object containing process results
*/
@interface IDVMRZCaptureResult : NSObject

@property (readwrite, strong) UIImage *mrzImage;
@property (readwrite, strong) IDVMRZQuadrangle *quadrangle;
@property (readwrite, strong) NSString *doc;
@property (readwrite, strong) NSString *doe;
@property (readwrite, strong) NSString *dob;
@property (readwrite, strong) NSMutableArray<NSMutableArray<NSValue*>*>* contours;
@property (nonatomic) int responseCode;

@end

#endif
