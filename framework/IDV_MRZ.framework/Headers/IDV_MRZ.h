//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IDV_MRZ.
FOUNDATION_EXPORT double IDV_MRZVersionNumber;

//! Project version string for IDV_MRZ.
FOUNDATION_EXPORT const unsigned char IDV_MRZVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import
// <IDV_MRZ/PublicHeader.h>
// <IDV_MRZ/PublicHeader.h>

#import <IDV_MRZ/IDVMRZCaptureCallback.h>
#import <IDV_MRZ/IDVMRZCaptureSDK.h>
#import <IDV_MRZ/IDVMRZCaptureResult.h>
#import <IDV_MRZ/IDVMRZConfiguration.h>
