//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVMRZConfiguration_h
#define IDVMRZConfiguration_h


/**
 Object containing configuration info to initialize the SDK.
*/
@interface IDVMRZConfiguration : NSObject {
    NSString *license;
}

+ (id)sharedIDVMRZConfiguration;
@property (nonatomic, readwrite) NSString *license;

@end

#endif
