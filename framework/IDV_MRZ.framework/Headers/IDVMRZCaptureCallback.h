//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef MRZCaptureCallback_h
#define MRZCaptureCallback_h

#import "IDVMRZCaptureResult.h"

/**
 SDK frame process delegate
*/
@protocol IDVMRZCaptureCallback <NSObject>

/**
 Callback for each frame processed without a valid detected MRZ
 @param partial IDVMRZCaptureResult reference with partial results.
*/
- (void) onProcessedFrame:(IDVMRZCaptureResult *)partial;

/**
 Callback called once when a valid detected MRZ
 @param result IDVMRZCaptureResult reference with results.
*/
- (void) onSuccess:(IDVMRZCaptureResult *)result;

@end

#endif
