#pragma once
#ifndef _IDVNFCConfigResult_h_
#define _IDVNFCConfigResult_h_

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


/**
 * PACE access protocol definition
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCPACEInfo: NSObject {
}
    @property (nonatomic, assign) NSInteger agreement;
    @property (nonatomic, assign) NSInteger mapping;
    @property (nonatomic, assign) NSInteger cipher;
    @property (nonatomic, assign) NSInteger cipherLen;
    @property (nonatomic, assign) NSInteger version;
    @property (nonatomic, assign) NSInteger parameter;
    @property (nonatomic, strong) NSString *description;
@end

/**
 * Class that returns the access configuration from the document.
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCConfigResult : NSObject {
}

    @property (nonatomic, assign) bool BACEnabled;
    @property (nonatomic, assign) bool PACEEnabled;

    /**
    * List of available protocols.
    */
    @property (nonatomic, strong) NSArray *protocols;

@end


#endif
