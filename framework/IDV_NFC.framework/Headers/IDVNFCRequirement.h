//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef NFCRequirement_h
#define NFCRequirement_h

#import <UIKit/UIKit.h>

@interface IDV_NFC_Requirement : NSObject

- (int)checkRequirements;
- (BOOL)checkArchitecture:(int)arch;
- (BOOL)checkOSVersion:(int)version;
- (BOOL)checkNFCHardware;

@end

#endif /* Requirement_h */
