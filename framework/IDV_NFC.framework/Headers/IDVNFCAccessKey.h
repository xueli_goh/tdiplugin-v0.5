#pragma once
#ifndef _IDVNFCAccessKey_h_
#define _IDVNFCAccessKey_h_


typedef NS_ENUM(NSInteger, IDVNFC_ACCESS_KEY) {
    IDVNFC_ACCESS_KEY_UNKNOWN = 0x00,
    IDVNFC_ACCESS_KEY_MRZ     = 0x01,
    IDVNFC_ACCESS_KEY_CAN     = 0x02,
    IDVNFC_ACCESS_KEY_PIN     = 0x03,
    IDVNFC_ACCESS_KEY_PUK     = 0x04
};

/**
 * Structure to provide access information.
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCAccessKey : NSObject {
    NSString *key;
    NSInteger keyType;
}

- (id)init:(NSString *)key type:(NSInteger)type;
- (NSString*) getString;
- (NSInteger) getType;

+ (IDVNFCAccessKey *) createMRZWithDOC:(NSString *)doc DOB:(NSString *)dob DOE:(NSString *)doe;
+ (IDVNFCAccessKey *) createCAN:(NSString *)can;
+ (IDVNFCAccessKey *) createPIN:(NSString *)pin;
+ (IDVNFCAccessKey *) createPUK:(NSString *)puk;

@end

#endif
