#pragma once
#ifndef _IDVNFCCapture_h_
#define _IDVNFCCapture_h_

#import <UIKit/UIKit.h>
#import <CoreNFC/CoreNFC.h>

#import <IDV_NFC/IDVNFCCaptureResult.h>
#import <IDV_NFC/IDVNFCConfigResult.h>
#import <IDV_NFC/IDVNFCAccessKey.h>


typedef NS_ENUM(NSInteger, IDVNFCProtocol) {
    IDVNFCProtocol_AUTO = -2,
    IDVNFCProtocol_BAC = -1
};

/**
 * Holds the identification code of each data group.
 */
typedef NS_ENUM(NSUInteger, IDVNFCCaptureDGId) {
    ID_DGCOM = 0x60,
    ID_DG1   = 0x61,
    ID_DG2   = 0x75,
    ID_DG3   = 0x63,
    ID_DG4   = 0x76,
    ID_DG5   = 0x65,
    ID_DG6   = 0x66,
    ID_DG7   = 0x67,
    ID_DG8   = 0x68,
    ID_DG9   = 0x69,
    ID_DG10  = 0x6A,
    ID_DG11  = 0x6B,
    ID_DG12  = 0x6C,
    ID_DG13  = 0x6D,
    ID_DG14  = 0x6E,
    ID_DG15  = 0x6F,
    ID_DG16  = 0x70,
    ID_DGSOD = 0x77
};

/**
 * Initialization error codes
 */
typedef NS_ENUM(NSUInteger, IDVNFCCaptureError) {
    /// No error code found
    IDVNFCCaptureErrorNone = 0,
    /// Invalid tag found
    IDVNFCCaptureErrorReadingChip = 1,
    /// Invalid tag found
    IDVNFCCaptureErrorInvalidTag = 2,
};

typedef NS_ENUM(NSUInteger, IDV_NFC_RequirementError) {
    /// No error code found
    IDV_NFC_RequirementErrorNone = 0,
    /// Architecture not supported
    IDV_NFC_RequirementErrorInvalidArchitecture = 1,
    /// Operating system not supported
    IDV_NFC_RequirementErrorInvalidOS = 2,
    /// Device without NFC reader technology
    IDV_NFC_RequirementErrorNFCNotAvailable = 3,
};

/**
 * Structure to store the progress
 */
typedef struct IDVNFCCaptureProgress {

    /**
     * Type of progress. (0 = Establishing communication, 1 = Reading data)
     */
    int   type;

    /**
     * Step on the progress. Only valid for type 1, there is one step for each data group to read.
     */
    int   step;

    /**
     * Global progress percentage (from 0 to 1)
     */
    float globalProgress;

    /**
     * Percentage read of current data group (from 0 to 1)
     */
    float stepProgress;
} IDVNFCCaptureProgress;

/**
 * Listener for SDK callbacks
 */
@protocol IDVNFCStartListener<NSObject> 

    /**
     * Called when the configuration is retrieved from the NFC.
     * @param configuration Object containing the NFC access protocols.
     */
    - (void) onConfiguration:(IDVNFCConfigResult*) configuration;

    /**
     * Called when starts is executed.
     * This method indicates the NFC read process is started.
     */
    - (void) onChipFound;

    /**
     * Called when the read process is finished.
     * This method provides the NFC read result.
     * @param result Object containing the NFC read results.
     */
    - (void) onResult:(IDVNFCCaptureResult*) result;

    /**
     * Called when a problem on the process is found.
     * This method indicates a system error while reading the NFC.
     * @param error Error description
     */
    - (void) onError:(NSError *)error;

@optional

    /**
     * Called when a progress update is done.
     * This methos indicates the progress on the NFC read process.
     * @param progress Object containig the progress data
     */
    - (void) onProgress:(IDVNFCCaptureProgress) progress;
@end

/**
 * This class reads the electronic chip on documents.
 */
__attribute__ ((visibility ("default")))  @interface IDVNFCCaptureSDK : NSObject<NFCTagReaderSessionDelegate>

/**
 * @return current SDK version as NSString
 */
+ (NSString*) version;

typedef void (^onInitBlock)(BOOL isCompleted, NSInteger errorCode);


/**
 * Initializes the SDK.
 * Call this method before {@link #start(IDVNFCBACKey, IDVNFCStartListener) start} method.
 * @param license Valid license content to run the SDK.
 * @param onInit This block will be called when the initialization is complete.
 */
- (void) init:(NSString *)license onInit:(onInitBlock)onInit;


/**
 * Retrieves the information required to set the PACE protocol.
 * @param listener The object listening the callbacks.
 */
-(void) configuration:(id<IDVNFCStartListener>)listener;

/**
 * Starts the process of NFC read. 
 * Call this method after {@link #init(String, onInitBlock) init} method.
 * This method will return inmediatelly, the result will be returned through the listener.
 * @param key Key object containing the document's number, the document's date of birth and the document's date of expiration.
 * @param listener The object listening the callbacks.
 */
- (void) start:(IDVNFCAccessKey *)key withBlock:(id<IDVNFCStartListener>)listener;
- (void) start:(IDVNFCAccessKey *)key withProtocolIndex:(NSInteger) idx withBlock:(id<IDVNFCStartListener>)listener;

/**
 * This method stops the NFC read process.
 * After this call, the method {@link #start(IDVNFCBACKey, IDVNFCStartListener) start} can be called again.
 */
- (void) stop;

/**
 * This method finishes the SDK and return is to an unitialized state.
 * Call this method before leave the activity that initialized the SDK.
 */
- (void) finish;

/**
 * This method set the message on the system popup.
 * @param msg String to show on the system popup
 */
- (void) setMessage: (NSString *)msg;

@end
#endif
