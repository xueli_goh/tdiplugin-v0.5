/*
 * Copyright (c) 2017 GEMALTO This computer program includes confidential and
 * proprietary information of Gemalto and is a trade secret of Gemalto. All use,
 * disclosure, and/or reproduction is prohibited unless authorized in writing by
 * Gemalto. All Rights Reserved.
 */

#ifndef medlmap_h
#define medlmap_h


// MEDLSTATUS gto_medl_initLib(void);
// SHA1:9becd59033b216c1063a8cb94ab9f1042afbf209
#define gto_medl_initLib h9becd59033

// MEDLSTATUS gto_medl_doneLib(void);
// SHA1:4da71c99b2817d36c403a5324cce25ece5d4f013
#define gto_medl_doneLib h4da71c99b2

// MEDLSTATUS gto_medl_free(MEDLByteArray**)
// SHA1:53978a074d3906ca298b272b647e8901e369651f
#define gto_medl_free h53978a074d

// MEDLSTATUS gto_medl_detectJailbreak(MEDLByteArray**,MEDLByteArray**,uint32_t)
// SHA1:fca93eef9d729f7e22c1dee95ccaeb53cdc5e27d
#define gto_medl_detectJailbreak hfca93eef9d

// MEDLSTATUS gto_medl_detectHookNative(const MEDLNativeObj*,MEDLByteArray**,MEDLByteArray**)
// SHA1:084a0d397e5f303e6c16504978d952ca16313117
#define gto_medl_detectHookNative h084a0d397e

// MEDLSTATUS gto_medl_detectHookMessage(const MEDLSharedObj*,MEDLByteArray**,MEDLByteArray**)
// SHA1:36a75a4248a871bee3447fb2cb609c784abc5e13
#define gto_medl_detectHookMessage h36a75a4248


#endif /* medlmap_h */
