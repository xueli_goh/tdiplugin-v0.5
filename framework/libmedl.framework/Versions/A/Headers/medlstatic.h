/* ----------------------------------------------------------------------------
 *
 *     Copyright (c) 2017  -  GEMALTO DEVELOPEMENT - R&D
 *
 * -----------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT D&ESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * -----------------------------------------------------------------------------
 */

/**
 * @mainpage MEDL
 *
 * @section sec_intro Introduction
 * The Mobile Environment Detection Library (MEDL) provides services
 * to detect whether your device is compromised. <br>
 *
 * A device is exploited or compromised if:
 * - It is jailbroken, and/or
 * - It is hooked.
 *
 * @subsection sec_api_ref API Reference
 * - medlstatic.h list of API in MEDL
 *
 * @subsection sec_howitworks How it works
 * MEDL puts the jailbreak/hook detection status into two byte arrays.<br>
 * - \p outpos contains the position of first & second threshold and result.<br>
 * - \p outdata contains the value of first & second threshold and result.<br>
 *
 * Developer shall use \p outpos as position locator to find the values
 * in \p outdata.
 *  <table>
 *      <caption>\p outpos Description</caption>
 *      <tr>
 *          <th>#</th>
 *          <th>Details</th>
 *          <th>Description</th>
 *      </tr>
 *      <tr>
 *          <td align="center" width="50px">0</td>
 *          <td width="150px">outpos->pData[0]</td>
 *          <td width="300px">Position of first threshold in \p outdata</td>
 *      </tr>
 *      <tr>
 *          <td align="center">1</td>
 *          <td>outpos->pData[1]</td>
 *          <td>Position of second threshold in \p outdata</td>
 *      </tr>
 *      <tr>
 *          <td align="center">2</td>
 *          <td>outpos->pData[2]</td>
 *          <td>Position of result in \p outdata</td>
 *      </tr>
 *  </table>
 *
 * @subsection sec_result_means What the result means
 * Developer shall compare the result with first (th1) and second (th2) threshold
 * to retrieve the jailbreak/hook detection status.<br>
 *
 *  <table>
 *      <caption id="detection_status">Jailbreak/Hook Detection Status Description</caption>
 *      <tr>
 *          <th>Result Range</th>
 *          <th>Jailbreak Detection</th>
 *          <th>Hook Detection</th>
 *      </tr>
 *      <tr align="center">
 *          <td width="160px">0 <= result < th1</td>
 *          <td width="170px">Jailbroken</td>
 *          <td width="170px">Hooked</td>
 *      </tr>
 *      <tr align="center">
 *          <td>th1 <= result < th2</td>
 *          <td colspan="2">Suspicious</td>
 *      </tr>
 *      <tr align="center">
 *          <td>th2 <= result <= 100</td>
 *          <td>Not jailbroken</td>
 *          <td>Not hooked</td>
 *      </tr>
 *  </table>
 * Please refer to "Dev Guide" to get more details of the data structure
 * and sample code<br>
 *
 * ----------------------------------------------------------------------------
 */
#ifndef medlstatic_h
#define medlstatic_h

#ifdef __cplusplus
extern "C" {
#endif

#include "medlmap.h"
#include <objc/message.h>
#include "medldatatype.h"


/******************************************************************************
 *                             Public API
 ******************************************************************************/

/**
 * @brief           Initialize library.
 *
 * Function that initialize library before calling these functions:
 * - ::gto_medl_detectJailbreak (optional)
 * - ::gto_medl_detectHookMessage
 * - ::gto_medl_detectHookNative
 *
 * @note            If library has been initialized, it returns
 *                  ::MEDLSTATUS_SUCCESS immediately.
 *
 * @return          Status of the initialization operation ::MEDLSTATUS.
 *
 * @retval          ::MEDLSTATUS_SUCCESS
 *                  Initialization is successful.
 *
 * @retval          ::MEDLSTATUS_FAILED
 *                  Initialization is failed.
 *
 * @see             ::gto_medl_doneLib
 *
 * @since           2.2.0
 *
 */
MEDLSTATUS gto_medl_initLib(void);

/**
 * @brief           Wipe library data in memory.
 *
 * Function that wipe data in memory which is used by the library.
 *
 * For security reason, call this function at the end of usage.
 *
 * @note            Calling this function will uninitialize library.
 *                  To reinitialize library use ::gto_medl_initLib.
 *
 * @return          Status of the operation ::MEDLSTATUS.
 *
 * @retval          ::MEDLSTATUS_SUCCESS
 *                  Operation is successful.
 *
 * @see             ::gto_medl_initLib
 *
 * @since           2.2.0
 *
 */
MEDLSTATUS gto_medl_doneLib(void);

/**
 * @brief           Free data MEDLByteArray memory allocation.
 *
 * Function that securely wipe sensitive data and also free the allocated
 * memory.
 *
 * For security reason, call this function immediately
 * after every calling jailbreak/hook detection API:
 * - ::gto_medl_detectJailbreak
 * - ::gto_medl_detectHookMessage
 * - ::gto_medl_detectHookNative
 *
 * @param[in,out]   data
 *                  Address of a pointer variable to MEDLByteArray
 *                  which will be deallocated by the library. <br>
 *
 * @note            Upon successful operation, <tt>data</tt> will be set to NULL.
 *
 * @return          Status of the operation ::MEDLSTATUS.
 *
 * @retval          ::MEDLSTATUS_SUCCESS
 *                  Deallocation is successful.
 *
 * @retval          ::MEDLSTATUS_INVALID_ARGUMENTS
 *                  Invalid argument(s), which might be due to:
 *                  - <tt>data</tt> is NULL.
 *                  - <tt>data</tt> is a pointer to NULL.
 *
 * @attention       If argument(s) is invalid :
 *                  - For version 2.2.0, it returns MEDLSTATUS_INVALID_ARGUMENTS.
 *                  - For earlier version, it returns MEDLSTATUS_FAILED.
 *
 * @see             ::cl
 *
 */
MEDLSTATUS gto_medl_free(MEDLByteArray ** data);


/**
 * @brief           Detect whether device is jailbroken.
 *
 * This function performs memory allocation for <tt>outdata</tt>
 * and <tt>outpos</tt>.
 * @note            It is the caller responsibility to call ::gto_medl_free
 *                  in order to free the allocated memory (<tt>outdata</tt> and
 *                  <tt>outpos</tt>).
 *
 * Explicitly calling ::gto_medl_initLib and ::gto_medl_doneLib is not necessary.
 *
 * @param[out]      outdata
 *                  Address of a pointer variable to ::MEDLByteArray that
 *                  contains first & second threshold and result.
 *
 * @param[out]      outpos
 *                  Address of a pointer variable to ::MEDLByteArray that
 *                  contains position of first & second threshold and result
 *                  in <tt>outdata</tt>.
 *
 * @param[in]       dataLen
 *                  Length of <tt>outdata</tt> to be allocated by this function.
 *
 * @attention       Since version 2.2.0, dataLen must be 128.<br>
 *                  For earlier version, dataLen must be more than 128.
 *
 * @return          ::MEDLSTATUS
 *                  Status of the operation ::MEDLSTATUS.
 *
 * @retval          ::MEDLSTATUS_SUCCESS
 *                  Operation is successful.<br>
 *                  Please check <tt>outdata</tt> and <tt>outpos</tt> to know
 *                  @ref detection_status "jaibreak detection status".
 *
 * @retval          ::MEDLSTATUS_INVALID_ARGUMENTS
 *                  Invalid argument(s), which might be due to:
 *                  - <tt>outdata</tt> / <tt>outpos</tt> is NULL.
 *                  - <tt>outdata</tt> / <tt>outpos</tt> does not point to NULL.
 *                  - <tt>dataLen</tt> is not 128.
 *
 * @retval          ::MEDLSTATUS_FAILED
 *                  The operation failed, which might be due to:
 *                  - fail to compute detecton result.
 *
 * @see             ::xn
 *
 */
MEDLSTATUS gto_medl_detectJailbreak(MEDLByteArray ** outdata, MEDLByteArray ** outpos, uint32_t dataLen);


/**
 * @brief           Detect whether a C style or a pure Swift function is being hooked.
 *
 * The hook detection algorithm currently detects Cydia Substrate hooking.
 *
 * This function performs memory allocation for <tt>outdata</tt>
 * and <tt>outpos</tt>.
 * @note            It is the caller responsibility to call ::gto_medl_free
 *                  in order to free the allocated memory (<tt>outdata</tt> and
 *                  <tt>outpos</tt>).
 *
 * @param[in]       nativeObj
 *                  A MEDLNativeObj object for native hook checking.
 *                  - <tt>nativeObj->funcs</tt> cannot be NULL.
 *                  - <tt>nativeObj->funcsize</tt> cannot be zero.
 *                  - The caller must make sure that <tt>nativeObj->funcsize</tt>
 *                  has the correct value.<br>
 *                  If it's greater than funcs exact length size, it may crash.<br>
 *                  If smaller than funcs exact size, the functions will not be
 *                  fully covered.
 *
 * @param[out]      outdata
 *                  Address of a pointer variable to ::MEDLByteArray that
 *                  contains first & second threshold and result.
 *
 * @param[out]      outpos
 *                  Address of a pointer variable to ::MEDLByteArray that
 *                  contains position of first & second threshold and result
 *                  in <tt>outdata</tt>.
 *
 * @return          ::MEDLSTATUS
 *                  Status of the operation ::MEDLSTATUS.
 *
 * @retval          ::MEDLSTATUS_SUCCESS
 *                  Operation is successful.<br>
 *                  Please check <tt>outdata</tt> and <tt>outpos</tt> to know
 *                  @ref detection_status "hook detection status".
 *
 * @retval          ::MEDLSTATUS_NOTINIT_ERROR
 *                  Library is not initialized. To initialize library use
 *                  ::gto_medl_initLib.
 *
 * @retval          ::MEDLSTATUS_INVALID_ARGUMENTS
 *                  Invalid argument(s), which might be due to:
 *                  - <tt>nativeObj</tt> is NULL.
 *                  - <tt>nativeObj->funcsize</tt> is zero.
 *                  - all the functions inside of <tt>nativeObj->funcs</tt>
 *                  are not valid.
 *                  - <tt>outdata</tt> / <tt>outpos</tt> is NULL.
 *                  - <tt>outdata</tt> / <tt>outpos</tt> does not point to NULL.
 *
 * @retval          ::MEDLSTATUS_FAILED
 *                  Operation is failed, which might be due to:
 *                  - Fail to compute detecton result.
 *                  - Internal call (e.g: dladdr) failed.
 *
 * @see             ::gto_medl_detectHookMessage
 *
 * @since           2.2.0
 *
 */
MEDLSTATUS gto_medl_detectHookNative(const MEDLNativeObj *nativeObj, MEDLByteArray ** outdata, MEDLByteArray ** outpos);


/**
 * @brief           Detect whether Objective-C message is being hooked.
 *
 * Function to detect whether the user given selectors and class is being hooked.
 * The hook detection algorithm currently detects Cydia Substrate hooking.
 * It is able to detect for Objective-C and Swift-ObjC functions.
 *
 * @warning         Do not pass 'Category' class, System APIs and Swizzling
 *                  method as it is NOT supported.
 *                  The checking result may not correct
 *
 * This function performs memory allocation for <tt>outdata</tt>
 * and <tt>outpos</tt>.
 *
 * @note            It is the caller responsibility to call ::gto_medl_free
 *                  in order to free the allocated memory (<tt>outdata</tt> and
 *                  <tt>outpos</tt>).
 *
 * @param[in]       sharedObj
 *                  A MEDLSharedObj object for objective-C hook checking.
 *                  - <tt>sharedObj->clazz</tt> must not be NULL.
 *                  - If <tt>sharedObj->sels</tt> is set to NULL, it checks all
 *                  the methods in <tt>sharedObj->clazz</tt>.
 *                      + <tt>sharedObj->selsize</tt> must be set to zero.
 *                  - If <tt>sharedObj->sels</tt> != NULL, the caller must make
 *                  sure to pass the correct value
 *                  <tt>sharedObj->selsize</tt>.<br>
 *                  If <tt>sharedObj->selsize</tt> is greater than funcs exact
 *                  length size, it may occur crash.<br>
 *                  If <tt>sharedObj->selsize</tt> is smaller than funcs exact
 *                  size, the functions will not be fully covered.
 *                  - If the given selector in <tt>sharedObj->sels</tt> cannot
 *                  be found in the <tt>sharedObj->clazz</tt>, library continues
 *                  to look in parent class (up to NSObject).
 *
 * @param[out]      outdata
 *                  Address of a pointer variable to ::MEDLByteArray that
 *                  contains first & second threshold and result.
 *
 * @param[out]      outpos
 *                  Address of a pointer variable to ::MEDLByteArray that
 *                  contains position of first & second threshold and result
 *                  in <tt>outdata</tt>.
 *
 * @return          ::MEDLSTATUS
 *                  Status of the operation ::MEDLSTATUS.
 *
 * @retval          ::MEDLSTATUS_SUCCESS
 *                  Operation is successful.<br>
 *                  Please check <tt>outdata</tt> and <tt>outpos</tt> to know
 *                  @ref detection_status "hook detection status".
 *
 * @retval          ::MEDLSTATUS_NOTINIT_ERROR
 *                  Library is not initialized. To initialize library use
 *                  ::gto_medl_initLib.
 *
 * @retval          ::MEDLSTATUS_INVALID_ARGUMENTS
 *                  Invalid argument(s) which might be due to:
 *                  - <tt>sharedObj</tt> is NULL.
 *                  - <tt>sharedObj->clazz</tt> is NULL.
 *                  - <tt>outdata</tt> / <tt>outpos</tt> is NULL.
 *                  - <tt>outdata</tt> / <tt>outpos</tt> does not point to NULL.
 *
 * @retval          ::MEDLSTATUS_FAILED
 *                  Operation is failed, which might be due to:
 *                  - Fail to compute detecton result.
 *                  - Internal call (e.g: dladdr) failed.
 *
 * @see             ::gto_medl_detectHookNative
 *
 * @since           2.2.0
 *
 */
MEDLSTATUS gto_medl_detectHookMessage(const MEDLSharedObj *sharedObj, MEDLByteArray ** outdata, MEDLByteArray ** outpos);

/**********************************************************************************
 *                             Public Macros
 **********************************************************************************/

/**
 * @brief           Returns if the app is running on a jailbroken phone.
 * 
 * Can only be used in a gcc or clang compiler.<br>
 * Exits immediately in the child process.
 *
 * @hideinitializer
 *
 * @return          @c true if the phone is jailbroken, @c false if not
 *
 * @since           2.2.0
 */
#if TARGET_IPHONE_SIMULATOR
// fork is supported on the simulator, but it cannot be jailbroken
#define GTO_MEDL_ISJAILBROKEN_01() false
#else
#define GTO_MEDL_ISJAILBROKEN_01() \
({\
    int _fork_result = fork();\
    if (_fork_result == 0)\
        exit(0);\
    _fork_result > 0;\
})
#endif

/**
 * @brief           Detect whether device is jailbroken.
 *
 * Function xn is now deprecated and is replaced by ::gto_medl_detectJailbreak.
 * This is to keep backward compatibility with previous version.
 *
 * @hideinitializer
 *
 * @see             ::gto_medl_detectJailbreak
 *
 * @deprecated      2.2.0
 */
#define xn gto_medl_detectJailbreak

/**
 * @brief           Free data MEDLByteArray memory allocation.
 *
 * Function cl is now deprecated and is replaced by ::gto_medl_free.
 * This is to keep backward compatibility with previous version.
 *
 * @hideinitializer
 *
 * @see             ::gto_medl_free
 *
 * @deprecated      2.2.0
 */
#define cl gto_medl_free

#ifdef __cplusplus
}
#endif

#endif /* medlstatic_h */
