/*
 * Copyright (c) 2017 GEMALTO This computer program includes confidential and
 * proprietary information of Gemalto and is a trade secret of Gemalto. All use,
 * disclosure, and/or reproduction is prohibited unless authorized in writing by
 * Gemalto. All Rights Reserved.
 */

#ifndef medldatatype_h
#define medldatatype_h

#include <stdint.h>
#include <objc/objc.h>


#ifdef __cplusplus
extern "C" {
#endif
    

#define MEDL_VERSION "2.2.0"
    
    /**
     * @brief Type definition for the structure used in handling return value and its length
     *
     * MEDLByteArray variables are used throughout the MEDL to represent return values
     *
     */
    typedef struct    {
        /** [mandatory] <BR>    the length of pData */
        uint32_t dataLen;
        /** [mandatory] <BR>    a pointer with unsigned char* datatype */
        unsigned char *pData;
    }   MEDLByteArray;
    
    
    /**
     * @brief Type definition for the structure used to pass the function list to gto_medl_detectHookNative
     *
     * MEDLNativeObj Native function list for gto_medl_detectHookNative
     *
     */
    typedef struct    {
        /** [mandatory] <BR>    The list of function pointers  */
        void ** funcs;
        /** [mandatory] <BR>    The count of the function pointers passed in the funcs  */
        size_t  funcsize;
    }   MEDLNativeObj;
    
    /**
     * @brief Type definition for the structure used to pass the class & method list to gto_medl_detectHookMessage
     *
     * MEDLSharedObj ObjC class & method list for gto_medl_detectHookMessage
     *
     */
    typedef struct    {
        /** [mandatory] <BR>    The class to be checked <BR>*/
        Class  clazz;
        /** [optional]  <BR>    The selectors'(methods) list */
        SEL *  sels;
        /** [optional]  <BR>    The count of selectors(methods) passed in the sels */
        size_t selsize;
    }   MEDLSharedObj;
    
    
    /**
     * @brief Enum type containing the possible status returned by the public APIs
     *
     */
    typedef enum {
        MEDLSTATUS_SUCCESS,             /**< 0 - Operation Succeeded  */
        MEDLSTATUS_FAILED,              /**< 1 - Operation failed and internal error */
        MEDLSTATUS_INVALID_ARGUMENTS,   /**< 2 - Invalid input parameters  */
        MEDLSTATUS_NOTINIT_ERROR        /**< 3 - Library Initialization failed  */
    }   MEDLSTATUS;
    
    
#ifdef __cplusplus
}
#endif


#endif /* medldatatype_h */
