module.exports = function(context) {
    var fs = require('fs');
    var path = require('path');
    var config_xml = path.join(context.opts.projectRoot, 'config.xml');
    var et = require('elementtree');

    var data = fs.readFileSync(config_xml).toString();
    var etree = et.parse(data);
    var app_id = etree.getroot().attrib.id;
    
    let logFile = 'platforms/android/app/src/main/java/cordova/plugin/tdi/PLog.kt';
    let customCaptorFile = 'platforms/android/app/src/main/java/cordova/plugin/tdi/CustomCaptor.kt';

    console.log("Running hooks script after_plugin_add for Android");

    var file = fs.readFileSync(logFile, 'utf8');
    var result = file.replace(/applicationId/g, app_id);
    fs.writeFileSync(logFile, result);
    file = fs.readFileSync(customCaptorFile, 'utf8');
    result = file.replace(/applicationId/g, app_id);
    fs.writeFileSync(customCaptorFile, result);

    console.log('Extracted app id: ' + app_id);
};
