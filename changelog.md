# Changelog

<br>

### 0.5.0
----
**10 Jun 2021**
>
1. Integrate TDI 1.3.1 android and 1.3.2 ios SDKs.
2. Added JPEG compression configuration for custom captor.
3. Fixed android custom captor on use of layout resource IDs from app package name.
4. Fixed android hook script to overwrite app package name in custom captor.
5. Updated required Kotlin and Swift versions in plugin.xml file.
6. Removed importing of SwiftSignatureView dependency for iOS app.
>
**Known Issues / Limitations:**

- Other issues inherited from previous versions.


<br>

### 0.4.1
----
**17 May 2021**
>
- Minor updates for android and ios plugin integration.
>
**Known Issues / Limitations:**

- Other issues inherited from previous versions.

<br>

### 0.4.0
----
**30 Apr 2021**
>
1. Integrate TDI 1.3.1 android and ios SDKs.
2. Updated tdiSdkInit, which is the preferred way to input license (ex: IDV license) as parameter for both android and ios.
3. Fixed ios contract/signature captor issue in previous version.
4. Custom captor to launch in landscape mode for both android and ios.
5. Remove activity related code and permission requests from TdiSdkAgent in android.
6. By default call profile API in newSession for both android and ios.
7. Logs printed based on build configuration for both android and ios.
8. Minor updates to captor configuration.
>
**Known Issues / Limitations:**

- Profile API in ios does not return "theme" property.

<br>

### 0.3.0 (beta01)
----
**01 Apr 2021**
>
1. Add full support for all default captors for both android and ios.
2. Fixed profile API converting raw data to json for both android and ios.
3. Fixed android custom captor layout conflict issue.
4. Updated custom captor ok, cancel images for android inline with ios.
>
**Known Issues / Limitations:**

1. iOS contract/signature captor launches with blank contract and not able to proceed next to get the signature.
2. NFC captor for android and ios not tested.
2. Profile API in ios does not return "theme" property.

<br>

### 0.2.0 (alpha02)
----
**09 Mar 2021**
>
1. Integrate TDI 1.3.0 android and ios SDKs.
2. Add custom captor with configurable document form factor for both android and ios.
3. Add captor configuration for default and custom captor.
>
**Known Issues / Limitations:**

1. Profile API does not return expected json data.
2. Few default captors like MRZ, NFC, contract captors not tested.

<br>

### 0.1.0 (alpha01)
----
**08 Feb 2021**
>
- Initial release with definition of basic plugin APIs.
>
**Known Issues / Limitations:**

- Minimal or no functionality implemented.
