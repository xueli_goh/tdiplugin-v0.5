
var exec = require('cordova/exec');
var PLUGIN_NAME = 'TdiPlugin';

exports.tdiSdkInit = function(LICENSE, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'tdiSdkInit', [LICENSE]);
};

exports.getProfile = function(BASE_URL, JWT_TOKEN, TENANT_ID, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'getProfile', [BASE_URL, JWT_TOKEN, TENANT_ID]);
};

exports.newSession = function(BASE_URL, SCENARIO_NAME, JWT_TOKEN, TENANT_ID, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'newSession', [BASE_URL, SCENARIO_NAME, JWT_TOKEN, TENANT_ID]);
};

exports.sessionStart = function(successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionStart', []);
};

exports.sessionResume = function(TASK_ID, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionResume', [TASK_ID]);
};

exports.sessionResult = function(successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionResult', []);
};

exports.sessionUpdate = function(TASK_ID, PAGES, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionUpdate', [TASK_ID, PAGES]);
};

exports.sessionStop = function(successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionStop', []);
};

exports.getCaptor = function(CONFIG, successCall, errorCall) {
	exec(successCall, errorCall, PLUGIN_NAME, 'getCaptor', [CONFIG]);
};

const generalConfig = Object.freeze({
    CAPTURE_TYPE: "CAPTURE_TYPE", // Refer doc for list of default captors from TDI
    EXTRA_CAPTOR_CALLBACK: "EXTRA_CAPTOR_CALLBACK", // Refer doc on captor callback section
    EXTRA_CAPTOR_SKIPPABLE: "EXTRA_CAPTOR_SKIPPABLE", // Optional config used in Non-ID/Voice captors to skip running of relevant workflow tasks

    // Config for Passport, ID Card, ICAO
    EXTRA_IDV_LICENSE: "EXTRA_IDV_LICENSE", // IDV license string, get from Thales IBS team
    EXTRA_CAPTURE_OVERLAY: "EXTRA_CAPTURE_OVERLAY", // Guiding frame during Capture - true or false
    EXTRA_CAPTURE_NB_SIDES: "EXTRA_CAPTURE_NB_SIDES", // No of pages, applicable for ID Card and ICAO
    EXTRA_CAPTURE_DETECTION_MODE: "EXTRA_CAPTURE_DETECTION_MODE", // MachineLearning, ImageProcessing
    EXTRA_CAPTURE_QUALITY_CHECK_BLUR: "EXTRA_CAPTURE_QUALITY_CHECK_BLUR", // Strict, Relaxed, Disabled
    EXTRA_CAPTURE_QUALITY_CHECK_GLARE: "EXTRA_CAPTURE_QUALITY_CHECK_GLARE", // Color, White, Disabled
    EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS: "EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS", // Relaxed, Disabled
    EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY: "EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY", // BlackAndWhite, Disabled

    // Config for Selfie
    EXTRA_CAPTURE_WORKFLOW: "EXTRA_CAPTURE_WORKFLOW", // Charlie2, Charlie4, Charlie6
    EXTRA_CAPTURE_SAVE_JPEG_QUALITY: "EXTRA_CAPTURE_SAVE_JPEG_QUALITY", // to true,
    EXTRA_CAPTURE_TIME_OUT: "EXTRA_CAPTURE_TIME_OUT", // to 0.0,
    EXTRA_CAPTURE_GET_PORTRAIT: "EXTRA_CAPTURE_GET_PORTRAIT", // to true,

    // Config for Signature
    EXTRA_SCROLL_BOTTOM_MANDATORY: "EXTRA_SCROLL_BOTTOM_MANDATORY", // to true,

    // Config for Non-ID
    EXTRA_CAPTURE_TIMEOUT: "EXTRA_CAPTURE_TIMEOUT", // to 120, set Timeout to 120s, 0 to disable timeout, default = 60s
    EXTRA_CAPTURE_RESOLUTION: "EXTRA_CAPTURE_RESOLUTION", // to 4, Image resolution in mega pixels, default= 4MP
    EXTRA_CAPTURE_QUALITY: "EXTRA_CAPTURE_QUALITY", // to 90, JPEG compression quality, default= 80

    // Config for MRZ
    EXTRA_MRZ_DISPLAY_HELP: "EXTRA_MRZ_DISPLAY_HELP", // to true, // Display or not help
    EXTRA_MRZ_CAPTURE_TIMEOUT: "EXTRA_MRZ_CAPTURE_TIMEOUT", // to 60, // Timeout, default 60s

    // Config for NFC
    EXTRA_NFC_DISPLAY_WAIT_TAP_ANIMATION: "EXTRA_NFC_DISPLAY_WAIT_TAP_ANIMATION", // to true, Display or not an animation while waiting after NFC tap
    EXTRA_NFC_DISPLAY_MOVE_DETECTION: "EXTRA_NFC_DISPLAY_MOVE_DETECTION", // to true, Display help in case of handset movement
    EXTRA_MRZ_INPUT_DOC_NUMBER: "EXTRA_MRZ_INPUT_DOC_NUMBER", // to "xxxxxxxxxx", Optional if MRZ captor is used before NFC reading
    EXTRA_MRZ_INPUT_DATE_OF_BIRTHDAY: "EXTRA_MRZ_INPUT_DATE_OF_BIRTHDAY", // to "YYMMDD", Optional if MRZ captor is used before NFC reading
    EXTRA_MRZ_INPUT_DATE_OF_EXPIRATION: "EXTRA_MRZ_INPUT_DATE_OF_EXPIRATION", // to "YYMMDD", Optional if MRZ captor is used before NFC reading
    EXTRA_NFC_READING_TIMEOUT: "EXTRA_NFC_READING_TIMEOUT", // to 120, Timeout, default 60s

    // Config for Custom captor using IDV
    EXTRA_CAPTURE_MODE: "EXTRA_CAPTURE_MODE", // Choose document type, like "Custom"
    EXTRA_DOC_SIZE: "EXTRA_DOC_SIZE", // Document width and height
    EXTRA_CAPTURE_JPEG_COMPRESSION: "EXTRA_CAPTURE_JPEG_COMPRESSION" // Document image compression in JPEG format, default= 80. Expected value range 0-100. 0 meaning compress for small size, 100 meaning compress for max quality
});

const docConfig = Object.freeze({
   DETECTION_MODE: { MachineLearning: "MachineLearning", ImageProcessing: "ImageProcessing", Disabled: "Disabled" },
   BLUR: { Strict: "Strict", Relaxed: "Relaxed", Disabled: "Disabled" },
   GLARE: { Color: "Color", White: "White", Disabled: "Disabled"},
   DARKNESS: { Relaxed: "Relaxed", Disabled: "Disabled" },
   PHOTOCOPY: { BlackAndWhite: "BlackAndWhite", Disabled: "Disabled" }
});

const selfieConfig = Object.freeze({
    WORKFLOW: { Charlie2: "Charlie2", Charlie4: "Charlie4", Charlie6: "Charlie6" }
});

const customConfig = Object.freeze({
   DOC_SIZE: { Width: "Width", Height: "Height" }
});

exports.captureConfig = {
   generalConfig,
   docConfig,
   selfieConfig,
   customConfig
};

